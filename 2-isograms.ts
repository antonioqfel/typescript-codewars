/**
 * Isograms
 * 
 * An isogram is a word that has no repeating letters, consecutive or non-consecutive.
 * Implement a function that determines whether a string that contains only letters is an isogram. 
 * Assume the empty string is an isogram. Ignore letter case.
 * 
 * Examples:
 * 
 * isIsogram "Dermatoglyphics" == true
 * isIsogram "aba" == false
 * isIsogram "moOse" == false -- ignore letter case
 */

export function isIsogram (str: string): boolean {
    return (new Set(str.toLowerCase())).size === str.length
  }

export function isIsogram2(str: string): boolean {
    if (str === '') { return true; }
    
    let isogramWord: boolean = false;
    const uniqueLetters: string[] = [];
    const letters = str.split('').map(letter => letter.toLowerCase());
    
    for (const letter of letters) {
      if (!uniqueLetters.includes(letter)) {
          uniqueLetters.push(letter);
          isogramWord = true;
          continue;
      }
      isogramWord = false;
      break;  
    }
    return isogramWord;
  }